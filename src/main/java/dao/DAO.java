package dao;

import factory.Connect;
import org.testng.annotations.AfterClass;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DAO {

    Connect conexao = new Connect();
    Statement stmt = conexao.abrirConexaoBancoDados();

    @AfterClass
    public void fechaBancoDados() throws SQLException {
        stmt.close();
    }

    public List<Map<String, Object>> consultaPessoa() throws SQLException {
        String SQL = "SELECT Nome, Cpf FROM dbPESSOAS.dbo.Pessoa";
        ResultSet rs = stmt.executeQuery(SQL);
        Map<String, Object> linha = null;
        List<Map<String, Object>> lista = new ArrayList<>();

        while (rs.next()) {
           linha = new HashMap<String, Object>();
          linha.put("Nome", rs.getString("Nome"));
          linha.put("Cpf", rs.getString("Cpf"));
          lista.add(linha);
       }
       return lista;
    }

}