package factory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class Connect {

    Statement stmt = null;

    public Statement abrirConexaoBancoDados() {

        try {
            Connection con = DriverManager.getConnection("jdbc:sqlserver://192.168.0.212;user=usrpessoasadm;password=DbPeAdm20!");
            stmt = con.createStatement();

        }
        catch (SQLException e) {
            System.out.println("Erro ao tentar conectar no banco de dados.");
            e.printStackTrace();
        }
        return stmt;
    }

}